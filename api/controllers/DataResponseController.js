/**
 * DataResponseController
 *
 * @description :: Server-side logic for managing Dataresponses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	dataResponse: function(req,res){
		var filePath, filename = '';

		// full filePath to the google zip file
		filePath = 'assets/google_transit_data/google_transit.zip';

		// the name of the file to send back
		filename = 'google_transit.zip';

		console.log('setting attachment headers ...');

		// set the headers to tell the browers this is an attachment
		res.attachment(filePath); // doc = http://sailsjs.com/documentation/reference/response-res/res-attachment

		console.log('downloading file ...');

		// respond with the file
		res.download(filePath, filename, function(err){ // doc = http://expressjs.com/en/api.html#res.download
			if(err){ console.log('error with downloading the file! e = ' + err); }
			else { console.log('APPROVED with downloading the file!'); }
		});

		console.log('Done!');
	}

};
