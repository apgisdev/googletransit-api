# GoogleTransit #

---

## Synopsis

This is the Sails.js back end for Google Transit. It handles Google's automated or manual data grab for our version of Clarksville Transit System's data.

## Requirements
 + Node.js and NPM
 + Sails.js

## Installation (Manual)

1. If not already installed, install the latest version of sailsjs globally via npm.
```
$ npm install sails -g
```

2. Clone this repository.
```bash
& git clone https://SUPERharrison@bitbucket.org/apgisdev/googletransit-api.git
```

3. Navigate to the directory of the project and use
```
$ sails lift
```
to manually spin up a sails server on `localhost:1337`

4. Then in Chrome or Firefox go to
```
localhost:1337/dataresponse
```
to start the download the zip file of the data we're sending to Google.

5. Open the zip file and check if all or any of the data made it, If so you're good to go!

## Installation (Automatic)

1. Go to `https://partnerdash.google.com/`
2. Log In using the GIS Center's Google account
3. Click on the Grey Bus icon that says Google Transit
4. Click `FEED CONFIGURATION`

## Endpoints
This is a list of all endpoints designed to be exposed to queries and their purpose.
All run from the base url (Ex: http://localhost:1337/message)

* get /dataresponse': Download the data we're sending to Google

## License

TODO: A short snippet describing the license (MIT, Apache, etc.)

## Reference
Readme template from: [https://gist.github.com/jxson/1784669](https://gist.github.com/jxson/1784669)
