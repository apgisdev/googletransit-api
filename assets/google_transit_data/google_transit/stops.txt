stop_id,stop_name,stop_lat,stop_lon
S1,Transit Center,36.5279867,-87.3584311
S2,Providence Blvd. & Peachers Mill Rd.,36.5501314,-87.3825531
S3,Ft. Campbell Blvd. & Airport Rd.,36.6182014,-87.431053
S4,Durrett Center,36.640272,-87.434407
S5,Walmart North,36.5802974,-87.4128852
S6,Commissary & PXTRA,36.646041,-87.4522448
S7,Fort Campbell Gate 4,36.644680,-87.437152
